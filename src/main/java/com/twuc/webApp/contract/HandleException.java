package com.twuc.webApp.contract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleException {
    private final Logger logger = LoggerFactory.getLogger(HandleException.class);

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<ErrorMessage> getErrorMessage(IllegalArgumentException exception) {
        outputLogger(exception);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorMessage(exception.getMessage()));
    }

    void outputLogger(IllegalArgumentException exception) {
        logger.info("error：{}", exception.getMessage());
    }
}
