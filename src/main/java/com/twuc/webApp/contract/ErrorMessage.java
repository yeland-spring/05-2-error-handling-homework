package com.twuc.webApp.contract;

public class ErrorMessage {
    private String message;

    public ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ErrorMessage() {
    }
}
