package com.twuc.webApp.web;

import com.twuc.webApp.contract.ErrorMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class errorIntegrationTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_400_with_message_when_handle_exception() {
        ResponseEntity<ErrorMessage> entity = testRestTemplate.getForEntity("/mazes/color-solve?width=1", ErrorMessage.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        assertEquals("The width(1) is not valid", entity.getBody().getMessage());
    }
}
